#!/bin/bash -e

distro=$1
mirror=${2:-http://http.kali.org/kali}

keyring_url="http://http.kali.org/pool/main/k/kali-archive-keyring/"
keyring_file=$(curl -s -k $keyring_url | grep -oe "kali*.*all.deb" | sed -e 's/.*">//')
wget -nv $keyring_url/"$keyring_file"
dpkg -i "$keyring_file"
rm -f "$keyring_file"
keyring_file="/usr/share/keyrings/kali-archive-keyring.gpg"

for architecture in $ARCHS; do
  work_dir="$(pwd)/fakechroot/$architecture/$distro"
  rm -rf "$work_dir" || true
  mkdir -p "$work_dir"
  fakechroot fakeroot debootstrap --foreign --variant=fakechroot --components=main,contrib,non-free \
    --arch="$architecture" --include=kali-archive-keyring,kali-defaults \
    --keyring="$keyring_file" "$distro" "$work_dir" "$mirror"

  if [ "${architecture}" = "arm64" ]; then
    mkdir -p "$work_dir"/usr/bin/
    cp /usr/bin/qemu-aarch64-static "$work_dir"/usr/bin/
  elif [ "${architecture}" = "armhf" ]; then
    mkdir -p "$work_dir"/usr/bin/
    cp /usr/bin/qemu-arm-static "$work_dir"/usr/bin/
  fi

  cat > "$work_dir/usr/sbin/policy-rc.d" <<-'EOF'
	#!/bin/sh
	exit 101
EOF
  chmod +x "$work_dir/usr/sbin/policy-rc.d"

  #mount -t proc none "$work_dir"/proc || true
  #mount -t sysfs none "$work_dir"/sys || true
  #mount -t devpts pts "$work_dir"/dev/pts/ || true

 echo "int main() { return 0; }" | gcc -x c - -o "$work_dir"/usr/sbin/chown
 echo "int main() { return 0; }" | gcc -x c - -o "$work_dir"/usr/sbin/chmod
 echo "int main() { return 0; }" | gcc -x c - -o "$work_dir"/usr/sbin/chgrp
 echo "int main() { return 0; }" | gcc -x c - -o "$work_dir"/sbin/ldconfig
  
rootfs_chroot() {
  #export PATH=/usr/sbin:/sbin:$PATH
  fakechroot fakeroot chroot "$work_dir" "$@"
}

rootfs_chroot /debootstrap/debootstrap --second-stage || cat /builds/frangal/kali-docker/fakechroot/amd64/kali-rolling/debootstrap/debootstrap.log
#DEBOOTSTRAP_DIR=$work_dir/debootstrap debootstrap --second-stage --second-stage-target=$work_dir

  #umount "$work_dir"/proc || true
  #umount "$work_dir"/sys || true
  #umount "$work_dir"/dev/pts/ || true

  echo 'force-unsafe-io' > "$work_dir"/etc/dpkg/dpkg.cfg.d/docker-apt-speedup

  aptGetClean='"rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true";'
  cat > "$work_dir"/etc/apt/apt.conf.d/docker-clean <<-EOF
	DPkg::Post-Invoke { ${aptGetClean} };
	Dir::Cache::pkgcache "";
	Dir::Cache::srcpkgcache "";
EOF

  echo 'Acquire::Languages "none";' >"$work_dir"/etc/apt/apt.conf.d/docker-no-languages

  cat > "$work_dir"/etc/apt/apt.conf.d/docker-gzip-indexes <<-'EOF'
	Acquire::GzipIndexes "true";
	Acquire::CompressionTypes::Order:: "gz";
EOF

  echo 'Apt::AutoRemove::SuggestsImportant "false";' >"$work_dir"/etc/apt/apt.conf.d/docker-autoremove-suggests

  rm -rf "$work_dir"/usr/bin/qemu-* || true
  rm -rf "$work_dir"/var/lib/apt/lists/* || true
  rm -rf "$work_dir"/var/cache/apt/*.bin || true
  rm -rf "$work_dir"/var/cache/apt/archives/*.deb || true
  find "$work_dir"/var/log -depth -type f -print0 | xargs -0 truncate -s 0
  mkdir -p "$work_dir"/var/lib/apt/lists/partial

  echo "Creating ${architecture}.${distro}.tar.xz"
  tar -I 'pixz -1' -C "$work_dir" -pcf "${architecture}.${distro}".tar.xz .

  if [ -z "$CI_JOB_TOKEN" ]; then
    chmod 775 "${architecture}.${distro}".tar.xz
    rm -rf "${architecture}"
  fi

  if [ "$distro" = "kali-last-snapshot" ]; then
    awk -F= '$1=="VERSION" { print $2 ;}' "$work_dir"/usr/lib/os-release | tr -d '"' > release.version
  fi

done
